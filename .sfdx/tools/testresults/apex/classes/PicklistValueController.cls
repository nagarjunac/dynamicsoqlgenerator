/**********************************************************************************************
 ==============================================================================================
Purpose : Generate the Dynamic SOQL query which is going to implement in LWC
===============================================================================================
History :-

Author                   Date              Reference                     Description
Nagarjuna(PwC-India)    30/05/2023         NA                             Initial

***********************************************************************************************/

public with sharing class PicklistValueController {
    @AuraEnabled(cacheable=true)
    public static List<String> getObjectNames() {
        //Create a Empty list to store ObjectNames
        List<String> objectNames = new List<String>();
        //iterate the Objects
        for (Schema.SObjectType objectType : Schema.getGlobalDescribe().values()) {
            objectNames.add(objectType.getDescribe().getName());
        }
        //Sort the ObjectNames
        objectNames.sort();
        return objectNames;
    }

    @AuraEnabled(cacheable=true)
    public static List<String> getObjectFields(String objectName) {
        //Crate Empty List to store the FieldNames
        List<String> fieldNames = new List<String>();
        // To get field Name
        Schema.DescribeSObjectResult describeResult = Schema.getGlobalDescribe().get(objectName).getDescribe();
        Map<String, Schema.SObjectField> fieldsMap = describeResult.fields.getMap();
        for (String fieldName : fieldsMap.keySet()) {
            fieldNames.add(fieldName);
        }
        //Sort the FieldNames
        fieldNames.sort();
        return fieldNames;
    }

    @AuraEnabled(cacheable=true)
    //Create method for Query passing parameter values for ObjName,fieldNames and Filter condition
    public static List<sObject> executeQueryApex(String objectName, String fieldNames, String filterConditions) {
        //Create empty List for sObject
        List<sObject> queryResult = new List<sObject>();
        //Create a string Query
        String query = 'SELECT ' + fieldNames + ' FROM ' + objectName;
        
        if (!String.isBlank(filterConditions)) {
            query += ' WHERE ' + filterConditions;
        }

        
        //use try catch to handle any exception in the Query
        try {
            queryResult = Database.query(query);
        } catch (Exception e) {
            // Handle exception
        }
        return queryResult;
    }
    
}