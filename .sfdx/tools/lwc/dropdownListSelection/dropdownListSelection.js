/**********************************************************************************************
 ==============================================================================================
Purpose : Perform Logic on Dynamic SOQL Query which implements in HTML
===============================================================================================

***********************************************************************************************/


import { LightningElement,  track } from 'lwc';
import getObjectNames from '@salesforce/apex/PicklistValueController.getObjectNames';
import getObjectFields from '@salesforce/apex/PicklistValueController.getObjectFields';
import executeQueryApex from '@salesforce/apex/PicklistValueController.executeQueryApex';

export default class DropdownListSelection extends LightningElement {
    @track objectOptions = [];
    @track selectedObject;
    @track fieldOptions = [];
    @track selectedFields = [];
    @track filterConditions = '';
    @track queryResult;
    @track columns;

    connectedCallback() {
        getObjectNames()
            .then(result => {
                this.objectOptions = result.map(objectName => ({
                    label: objectName,
                    value: objectName
                }));
            })
            .catch(error => {
                // Handle error
            });
    }

    handleObjectChange(event) {
        this.selectedObject = event.detail.value;
        this.fieldOptions = []; // Clear previous field options

        getObjectFields({ objectName: this.selectedObject })
            .then(result => {
                this.fieldOptions = result.map(fieldName => ({
                    label: fieldName,
                    value: fieldName
                }));
            })
            .catch(error => {
                // Handle error
            });
    }

    handleFieldChange(event) {
        this.selectedFields = event.detail.value;
    }

    handleFilterChange(event) {
        this.filterConditions = event.target.value;
    }

    executeQuery() {
        executeQueryApex({ objectName: this.selectedObject, fieldNames: this.selectedFields.join(','), filterConditions: this.filterConditions })
            .then(result => {
                this.queryResult = result;
                this.columns = this.generateColumns(result);
            })
            .catch(error => {
                // Handle error
            });
    }

    generateColumns(queryResult) {
        let columns = [];
        if (queryResult.length > 0) {
            const fields = Object.keys(queryResult[0]);
            columns = fields.map(field => ({
                label: field,
                fieldName: field,
                type: 'text'
            }));
        }
        return columns;
    }
}